package p900;
import org.tatools.sunshine.testng.*;
import org.tatools.sunshine.core.*;
import p900.Tests.*;

public class TestRunner {
    public static void main(String[] args) {
//        new Sun(
//                new TestNGKernel(
//                        new LoadableTestNGSuite(
//                                new RegexCondition("^p900.Tests(.+)?")
//                        )
//                )
//        ).shine();
        new Sun(
                new TestNGKernel(
                        new LoadableTestNGSuite(
                                new SuiteFromClasses(
                                        googleTest.class,
                                        p900Test.class,
                                        bingTest.class
                                )
                        )
                )
        ).shine();
    }
}
