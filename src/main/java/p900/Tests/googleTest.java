package p900.Tests;

import com.codeborne.selenide.Condition;
import org.testng.annotations.Test;
import org.testng.Assert;
import p900.WebDriverTestBase;

import static com.codeborne.selenide.Selenide.*;

public class googleTest extends WebDriverTestBase{
    @Test
    public void openPage() {
        open("http://google.com");
        $("body").shouldBe(Condition.visible);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
